import 'package:crud_dog_mockdata/dog_service.dart';
import 'package:flutter/material.dart';

import 'dog.dart';
import 'dog_app_widget.dart';

void main() {
  runApp(MaterialApp(title: 'Dog App', home: DogApp()));
}
